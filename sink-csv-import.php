<?php
/* 
Plugin Name: Social Ink Term Fields & Importer
Plugin URI: 
Author URI: http://www.social-ink.net
Version: 1.1
Author: Yonatan Reinberg of Social Ink
Description: Import taxonomy/term
Copyright 2014-15 Yonatan Reinberg (email : yoni [a t ] s o cia l-ink DOT net) - http://social-ink.net
** Uses File_CSV_DataSource php parser by Kazuyoshi Tlacaelel
*/
define('SINK_CSV_PATH', WP_PLUGIN_DIR . '/' . plugin_basename(dirname(__FILE__)));
define('SINK_CSV_OPTIONS_TERM_FIELDS' , '_sink_csv_term_fields');
add_post_type_support( 'page', 'excerpt' );
add_action('admin_menu', 'sink_csv_import_menu');	
	function sink_csv_import_menu() {  
		add_submenu_page( "tools.php", "SINK CSV Import", "SINK CSV Import", "manage_options", "SINKCSVImport", "sink_csv_import_admin" );	
	}  		
		function sink_csv_import_admin() {  include('views/admin.php');  } 	
	function sink_csv_add_term($term, $extras) {
		global $wpdb;
		$insert_term = array(
			'name'	=>	$term['sink-name'],
			'slug'	=>	($term["sink-slug"]!="") ? $term['sink-slug'] : sanitize_title($term['sink-name'])
		);
		$newterm = $wpdb->insert($wpdb->terms, $insert_term);
		if($newterm) {
			$insert_term_taxonomy = array(
							'term_id'	=>	$wpdb->insert_id,
							'taxonomy'	=>	$term['sink-taxonomy'],
							'description'=>	$term['sink-description'],
						);
			$newterm = $wpdb->insert($wpdb->term_taxonomy, $insert_term_taxonomy);
			if($newterm) {				
				if($extras) 								
					foreach($extras as $extra => $indx)
						sink_csv_add_term_value($extra, $term[$extra], $wpdb->insert_id);
			}
		}
		return $newterm;		
	}
	function sink_csv_add_term_value($key , $val , $term_taxonomy_id) {
		$key = sink_csv_check_prefix($key);		
		$sink_csv_field = get_option(SINK_CSV_OPTIONS_TERM_FIELDS) ? json_decode(get_option(SINK_CSV_OPTIONS_TERM_FIELDS), true) : array();
		$sink_csv_field[$key][$term_taxonomy_id] = $val;
		$sink_csv_field = json_encode($sink_csv_field);
		update_option(SINK_CSV_OPTIONS_TERM_FIELDS, $sink_csv_field);
	}
	function sink_csv_get_term_value($key, $term_taxonomy_id) {
		$key = sink_csv_check_prefix($key);
		$sink_csv_field = get_option(SINK_CSV_OPTIONS_TERM_FIELDS) ? json_decode(get_option(SINK_CSV_OPTIONS_TERM_FIELDS), true) : false;
		if($sink_csv_field) {
			$sink_csv_field = (isset($sink_csv_field[$key]) && isset($sink_csv_field[$key][$term_taxonomy_id])) ? $sink_csv_field[$key][$term_taxonomy_id] : false;
		}
		return $sink_csv_field;
	}
	function sink_csv_check_prefix($key) {
		$prefix = 'sink-field-';
		$prefix_check = strpos($key, $prefix);
		if($prefix_check !== false)
			$key = substr($key, $prefix_check + strlen($prefix));	
		return $key;
	}
?>
<?php
 if ('POST' == $_SERVER['REQUEST_METHOD']) {
		$msg = 'Could not import.  Either terms already exist or you chose an invalid CSV file.  Remember to format as a CSV, UTF-8 encoding.';
		if(!empty($_FILES['sink_csv_import'])) {
			$basic_headers = array('sink-taxonomy','sink-name','sink-slug','sink-description');
			$success = 0;
			require_once SINK_CSV_PATH . '/vendor/DataSource.php';
			
			$csv = new File_CSV_DataSource;
			$csv->load($_FILES['sink_csv_import']["tmp_name"]);
			$csv->symmetrize();
			$terms = $csv->connect();					
			if($terms && is_array($terms)) {
				$headers = $csv->getHeaders();
				$headers = $headers;
				$extras = array_diff($headers,$basic_headers);
				$extras = ($extras && !empty($extras)) ? array_flip($extras) : false;
				$headers = array_flip($headers);
				
				if(isset($headers['sink-name']) && isset($headers['sink-taxonomy'])) {							
					foreach($terms as $term) {
						$term_add = sink_csv_add_term($term, $extras);
						if($term_add)
							$success++;
					}
					if($success > 0)
						$msg = 'Success! ' . $success . ' new terms were added to the database.';		
					else
						$msg = 'No new terms added. Maybe they already existed?';
				} else
					$msg = 'You uploaded a CSV, but the headers we need aren\'t there. Check the instructions below.';
			} 
		}
		echo "<div class=\"updated\"><p><strong>$msg</strong></p></div>";
	}	
	?>
	
<div class="wrap sink_csv_importer">	
	<div class="metabox-holder">
		<div class="icon32" id="icon-options-general"><br></div>	
		<h2>Social Ink Term Fields & Importer</h2>
			
			<div id="post-body">
				<div id="post-body-content" class="has-sidebar-content">
					<div class="section">
						<h4>Choose a file</h4>		
						<form method="post" enctype="multipart/form-data" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
							<input type="file" class="regular-text code" id="sink_csv_import" name="sink_csv_import">
							<p class="submit">
								<input type="submit" class="button-primary" value="Import CSV" />
							</p>
						</form>		
					</div>
					<div class="section">
						<h4>Instructions</h4>
						<p>Create a CSV file encoded in UTF-8 and upload, using the format below. Terms will only be successfully added if they do not exist in slug form.</p>
						<p>Notes:</p>
						<dd>
							<li>You may choose a custom slug or leave empty and the system will create one.</li>
							<li>Columns may be in any order as long as they keep the headers.</li>
							<li>Add a term "custom field" (metadata) by adding any number of columns with prefix <i>"sink-field-"</i> (eg "sink-field-abbreviation", where the custom field will be simply "abbreviation"). You may then retrieve that field at any time with <code>sink_csv_get_term_value($key,$term_taxonomy_id)</code> or add/modify it with <code>sink_csv_add_term_value($key,$val,$term_taxonomy_id)</code>.</li>
						</dd>							
						<h4>Table formatting</h4>
						<table class="widefat">
							<thead>
								<tr>
									<th><i>sink-taxonomy</i></th>
									<th><i>sink-slug</i></th>
									<th><i>sink-name</i></th>
									<th><i>sink-description</i></th>
									<th><i>sink-field-xxx</i></th>
								</tr>
							</thead>								
							<tbody>
								<tr>
									<td>Taxonomy Slug</td>
									<td>New Term Slug</td>
									<td>New Term Name</td>
									<td>New Term Description</td>
									<td>New Term Custom Field</td>
								</tr>
							</tbody>
							</table>	
					</div>
					<div style="background: none repeat scroll 0 0 #EEEEEE; border: 1px solid #F8F8F8; border-radius: 11px 11px 11px 11px; margin-bottom: 15px; margin-top: 15px; padding: 5px 5px 5px 15px; width: 800px;">
						<p>Copyright 2013-<? echo date('Y') ?> Yonatan Reinberg for Social Ink.  We do custom WordPress plugins, themes and sites big and small.</p>
						<p>To learn more about Social Ink, our people &amp; our projects, please visit our main site at <a style="color:#29ABE2" href="http://www.social-ink.net">social-ink.net</a>.</p>
					</div>				
				</div>		
			</div>
		</div>	
</div>